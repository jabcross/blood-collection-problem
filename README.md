# Blood collection problem

This is an implementation of the GRH algorithm for the Blood Collection Problem.

The Blood Collection problem is an extension of the routing problem, where we must route blood
donations from donation sites and deliver them to a processing center before a set expiration time
after which each donation spoils. The objective is to maximize the amount of blood processed.

This problem is described in the paper _Managing platelet supply through improved routing of blood
collection vehicles_, by Okan Örsan Özener and Ali Ekici.