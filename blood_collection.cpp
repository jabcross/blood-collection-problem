#include <algorithm>
#include <iostream>
#include <cstring>
#include <random>
#include <unordered_set>
#include <unordered_map>
#include <assert.h>
#include <bitset>
#include "utilities.h"

#define NUM_SITES (30)
#define NUM_VEHICLES (7)
#define MAX_DONATIONS_PER_SITE (50)
#define MAX_CLUSTER_SIZE (8)
#define TIME_TO_SPOIL (300) /* K */
#define T1 (0)
#define T2 (800)
#define MAX_TIME_UNITS
#define LAMBDA (50)
#define MEMOIZE (false)

// #define dprintf(...) {if (dprint) {for (int i = 0; i < indent_level; i++) printf(" "); printf(__VA_ARGS__);}}
#define dprintf(...) {}

int indent_level = 0;
bool dprint = true;

typedef struct _Data {
    int farthest_from_processing_center;
    int last_closing_time;
    int first_opening_time;
    int opening_and_closing_times [NUM_SITES*2];
    int distance_data[NUM_SITES * NUM_SITES];
    int donation_data[NUM_SITES * MAX_DONATIONS_PER_SITE];
    int donation_count[NUM_SITES];

    public:

    int get_distance(int site_a, int site_b){
        return distance_data[site_a * NUM_SITES + site_b];
    }
    void set_distance(int site_a, int site_b, int distance){
        distance_data[site_a * NUM_SITES + site_b] = distance;
        distance_data[site_b * NUM_SITES + site_a] = distance;
    }

    int get_donation(int site, int index){
        return donation_data[site * MAX_DONATIONS_PER_SITE + index];
    }
    void set_donation(int site, int index, int time){
        donation_data[site * MAX_DONATIONS_PER_SITE + index] = time;
    }

} Data;


void generate_dataset(Data* data) {

    memset(data, 0, sizeof(Data));

    // Make 30 positions in 2D space.
    double x_positions[NUM_SITES];
    double y_positions[NUM_SITES];

    // Half of the positions are uniformly distributed.
    for (int i = 1; i < NUM_SITES / 2; i++) {
        // uniform distribution
        double x = (double) rand() / RAND_MAX * 1000.0;
        double y = (double) rand() / RAND_MAX * 1000.0;
        x_positions[i] = x;
        y_positions[i] = y;
    }

    // The rest are clustered around 4 urban centers.
    double urban_center_xs[4] = {250.0, 250.0, 750.0, 750.0};
    double urban_center_ys[4] = {250.0, 750.0, 250.0, 750.0};
    for (int i = NUM_SITES / 2; i <= NUM_SITES; i++) {
        double x = urban_center_xs[i % 4] + gauss() * 500.0;
        double y = urban_center_ys[i % 4] + gauss() * 500.0;
        x_positions[i] = x;
        y_positions[i] = y;
    }

    // Find a random node to be the processing center.

    int processing_center = rand()%(NUM_SITES - 1) + 1;

    std::swap(x_positions[0],x_positions[processing_center]);
    std::swap(y_positions[0],y_positions[processing_center]);

    // Calculate the distances
    for (int i = 0; i < NUM_SITES; i++) {
        for (int j = 0; j <= i; j++) {
            if (i == j) {
                data->set_distance(i, j, 0);
                assert(data->get_distance(i,j) == 0);
            }
            else {
                int dist = (int) hypot(x_positions[i] - x_positions[j],
                                       y_positions[i] - y_positions[j]);
                data->set_distance(i,j,dist);
                assert(data->get_distance(i,j) == dist);
            }
        }
    }

    // Find the farthest node from the processing center.
    data->farthest_from_processing_center = -1;
    int farthest_dist = -1;
    for (int j = 0; j < NUM_SITES; j++) {
        if (data->get_distance(0,j) > farthest_dist) {
            farthest_dist = data->get_distance(0,j);
            data->farthest_from_processing_center = j;
        }
    }

    // Scale the distances (rounding up) so that the farthest donation is reachable

    double scale = (double)(T2 + TIME_TO_SPOIL - T1 + 1) / (double) (farthest_dist * 2.0);

    for (int i = 0; i < NUM_SITES; i++) {
        for (int j = 0; j < i; j++) {
            int old_distance = data->get_distance(i,j);
            double new_distance = ceil((double) old_distance * scale);
            data->set_distance(i,j, (int) new_distance);
        }
    }

    dprintf("Distances:\n");
    for (int i = 0; i < NUM_SITES; i++) {
        dprintf("%2d:: ", i);
        for (int j = 0; j < NUM_SITES; j++) {
            dprintf("%4d ", data->get_distance(i,j)); 
        }
        dprintf("\n");
    }


    // Generate the donations for each site.
    data->first_opening_time = INT32_MAX;
    data->last_closing_time  = INT32_MIN;
    for (int i = 1; i < NUM_SITES; i++) {
        // Each site has a uniform number of donations from 1 to 50.
        data->donation_count[i] = rand() % MAX_DONATIONS_PER_SITE + 1;
        bool uniform            = rand() % 2;
        int  opening            = random_range(T1, T2 / 3);
        int  closing            = random_range(2 * T2 / 3, T2);
        data->first_opening_time = std::min(opening, data->first_opening_time);
        data->last_closing_time  = std::max(closing, data->first_opening_time);
        int j = 0;
        for (; j < data->donation_count[i]; j++) {
            if (uniform) {
                // Uniformly choose a spot between these ranges.
                data->set_donation(i,j,random_range(opening, closing));
            } else {
                // Use a normal distribution centered on the middle of the open
                // time period.
                data->set_donation(i,j,
                    (int) (gauss() * (closing - opening) + (closing + opening) / 2));
            }
        }
        for (; j < MAX_DONATIONS_PER_SITE; j++){
            data->set_donation(i, j, T2);
        }

        int* beginning_of_list = data->donation_data + (i * MAX_DONATIONS_PER_SITE);
        int* end_of_list = data->donation_data +
                           i * MAX_DONATIONS_PER_SITE +
                           data->donation_count[i];
        std::sort(beginning_of_list, end_of_list);
        int* end_of_unique = &(*std::unique(beginning_of_list, end_of_list));
        data->donation_count[i] = end_of_unique - beginning_of_list;
    }

    dprintf("Donations:\n");
    for (int i = 1; i < NUM_SITES; i++) {
        dprintf("%2d (%2d):: ", i, data->donation_count[i]);
        for (int j = 0; j < data->donation_count[i]; j++){
            dprintf("%4d ", data->get_donation(i,j)); }
        dprintf("\n");
    }

    dprintf("Farthest from origin: %d at %d\n",
           data->farthest_from_processing_center,
           abs(data->get_distance(data->farthest_from_processing_center,0)));

    dprintf("End of data generation \n");
}


// Algorithm 1: Choosing the initial donation sites for each cluster.

// data = output of generate_data
// clusters = array of 32-bit sets for each vehicle
// remaining = set containing unassigned sites.
//
// Returns false if failed to find enough donations to create seed.
bool find_initial_donation_sites(Data& data, int* clusters, int& remaining) {
    dprintf("Generating seeds for clusters.\n");
    // 1: t' = T_2 + K
    // T2 is the latest closing time of any collection site.
    int t_prime = data.last_closing_time + TIME_TO_SPOIL;
    // Collected_i
    // first = count of collected donations, second = id of donation site
    std::vector<std::pair<int, int>> collected;
    // 2: repeat
    do {
        dprintf("t_prime = %d\n", t_prime);
        int positive = 0;
        collected.clear();
        // 3: Collected_i := Sum over (t'-K <= t <= t' - t_i0) d^i_t for all i
        // in V
        for (int i = 1; i < NUM_SITES;
             i++) {  // i = 0 is the processing center.
            int acc = 0;
            for (int donation = 0; donation < data.donation_count[i]; donation++) {
                if (data.get_donation(i,donation) >= t_prime - TIME_TO_SPOIL and
                    data.get_donation(i,donation) <= t_prime - data.get_distance(i,0)) {
                    acc += 1;
                }
            }
            collected.push_back(std::make_pair(acc, i));
            if (acc > 0) {
                positive += 1;
                dprintf("Collected_%d = %d\n",i,acc);
            }
        }
        // 4: if at least M donation sites have positive Collected_i then
        remaining = ~0;
        if (positive >= NUM_VEHICLES) {
            dprintf("Found enough positive Collected_i\n");
            // 5: Choose M donation sites with the largest Collected_i values as
            // the initial
            //    donation sites of each cluster.
            dprintf("Chosen cluster seeds: ");
            std::sort(collected.begin(), collected.end(), std::greater<std::pair<int,int>>());
            for (int i = 0; i < NUM_VEHICLES; i++) {
                clusters[i] = set_add(0,collected[i].second);
                dprintf("%d ",collected[i].second);
                remaining   = set_subtract(remaining, clusters[i]);                
            }
            dprintf("\n");

            return true;
        } else {
            // 6: else
            // 7: t' = t' - lambda
            t_prime -= LAMBDA;
            // 8: end if
        }
        // 9: until M donation sites are chosen
    } while (t_prime >= T1);
    dprintf("Did not find enough positive donation counts.\n");
    return false;
}

typedef struct {
    int node;
    int arrival_time;
} NodeArrival;

// Algorithm 3: Greedy heuristic strategy

// For a given cluster, maximize the amount of blood to be gathered by a single
// vehicle.

void greedy_heuristic(Data& data,
                     Set   cluster,
                     int&  maximum_collected,
                     int last_node,
                     int last_time,
                     std::vector<NodeArrival>* node_arrivals) {

    if ((cluster == 0) or (last_time < data.first_opening_time + TIME_TO_SPOIL)){
        maximum_collected = 0;
        return;
    }

    indent_level++;
    // Populating V_j.
    auto cluster_indexes = std::vector<int>();
    //dprintf("Calculating for cluster %s, at time %d with last node %d\n",to_list(cluster).c_str(), last_time, last_node);
    for (int i = 0; i < 32; i++) {
        if (set_has(cluster, i)) {
            cluster_indexes.push_back(i);
            }
    }
    assert(cluster_indexes.size() > 0);
    // 1: MaximumCollected := 0, Endtime := 0
    maximum_collected = 0;
    // 2: for t = T_2 + K to T1 - K do
    for (int t = last_time; t >= data.first_opening_time;
         t -= LAMBDA) {
        // 3: d`^i_l := d^i_l for all i in V_j , ; in {a_i , ... , b_i}
        // d` stores the donations that have been taken already.
        uint64_t d_prime[NUM_SITES];
        for (int i = 0; i < NUM_SITES; i++) d_prime[i] = 0;
        // 4: TempCollected := 0, Time := t, ArrivalTime := t
        // 5: ChosenNode := 0, PreviousNode := 0
        int temp_collected = 0, _time = t, arrival_time = t, chosen_node = last_node,
            previous_node = last_node;
        // 6: while Time >= T_1 - K do
        while (_time >= data.first_opening_time - TIME_TO_SPOIL and _time >= 0) {
            // 7: MaximumCollectible := max_(i in V_j)
            // sum_(l=ArrivalTime-K)^(Time-t_(i,PreviousNode))d`^i_j
            // 8: ChosenNode := argmax_(i in V_j)
            // sum_(l=ArrivalTime-K)^(Time-t_(i,PreviousNode))d`^i_j
            int maximum_collectible = 0;
            int chosen_node         = 0;
            uint64_t collected_donations_set = 0;
            for (int site : cluster_indexes) {
                if (site == previous_node) continue; // Must ignore current node (missing from pseudocode)
                int acc = 0;
                for (int donation = 0; donation < data.donation_count[site]; donation ++) {
                    bool has_been_taken = set_has(d_prime[site],donation);
                    int dist_to_prev_node = data.get_distance(site,previous_node);
                    if (!has_been_taken and
                        (data.get_donation(site,donation) <=
                         _time - dist_to_prev_node) and
                        (data.get_donation(site,donation) >= arrival_time - TIME_TO_SPOIL)) {
                        acc += 1;

                        collected_donations_set |= (1 << donation);
                    }
                }
                if (acc > maximum_collectible) {
                    maximum_collectible = acc;
                    chosen_node         = site;
                }
            }
            // 9: if MaximumCollectible = 0 & PreviousNode != 0 then
            if (maximum_collectible == 0 and previous_node != 0) {
                // Nowhere to go, let's end the tour.
                //dprintf("End tour\n");
                // Store the current arrival time.
                NodeArrival node_arrival = {previous_node, arrival_time};
                if (node_arrivals) node_arrivals->push_back(node_arrival);

                // 13: Time := ArrivalTime - lambda
                // 10: ChosenNode := 0, ArrivalTIme := Time - t_(ChosenNode,
                // PreviousNode)
                // 11: Time := ArrivalTime, PreviousNode := 0
                chosen_node   = 0;
                arrival_time  = _time - data.get_distance(0,previous_node);
                _time         = arrival_time;
                previous_node = 0;
            }
            // 12: else if MaximumCollectible = 0 and PreviousNode = 0 then
            else if (maximum_collectible == 0) {
                // Nowhere good to go from the processing center, let's wait a
                // bit.
                arrival_time = arrival_time - LAMBDA; // Bug in the pseudocode, let's update the arrival time on the processing center too.
                _time = arrival_time;
                continue;
            }
            // 14: else
            else {
                // There's somewhere to go! Let's go there.

                // Store the current arrival time.


                NodeArrival node_arrival = {previous_node, arrival_time};
                if (node_arrivals) node_arrivals->push_back(node_arrival);

                // 15: TempCollected := TempCollected + MaximumCollectible
                temp_collected += maximum_collectible;
                
                // 16: for l = ArrivalTime - K to Time - t_(ChosenNode,
                // PreviousNode) do
                // 17: d`^ChosenNode_l := 0
                // 18: end for
                d_prime[chosen_node] |= collected_donations_set;

                // 19: Time := Time - t_(ChosenNode, PreviousNode)
                _time -= data.get_distance(chosen_node,previous_node);
                //dprintf("Chosen node %d, grabbed %d donations at time %d, arrival time %d\n", chosen_node, maximum_collectible, _time, arrival_time);
                // 20: PreviousNode := ChosenNode
                previous_node = chosen_node;
                // 21: end if
            }
            // 22: end while
            // 23: if TempCollected > MaximumCollected then
            if (temp_collected > maximum_collected) {
                // 24: MaximumCollected := TempCollected, EndTime := t
                maximum_collected = temp_collected;
            }
            // 25: end if
        }
        // 26: end for
        //dprintf("Collected %d donations when arriving at time %d\n", temp_collected, t);
    }
    //dprintf("Received %d donations from cluster %s\n", maximum_collected, to_list(cluster).c_str());
    indent_level --;
}

void greedy_recursive_heuristic(Data& data,
                     Set   cluster,
                     int&  maximum_collected,
                     int last_node,
                     int last_time,
                     std::vector<NodeArrival>* node_arrivals) {

    indent_level ++;
    // If cluster is empty, return 0.

    if (cluster == 0){
        maximum_collected = 0;
        return;
    }

    // Populating V_j.
    auto cluster_indexes = std::vector<int>();
    //dprintf("Calculating for cluster %s\n",to_list(cluster).c_str());
    for (int i = 0; i < 32; i++) {
        if (set_has(cluster, i)) {
            cluster_indexes.push_back(i);
            }
    }
    assert(cluster_indexes.size() > 0);
    // 1: MaximumCollected := 0, Endtime := 0
    maximum_collected = 0; 
    // 2: for t = T_2 + K to T1 - K do
    for (int t = last_time; t >= data.first_opening_time;
         t -= LAMBDA) {
        // 3: d`^i_l := d^i_l for all i in V_j , ; in {a_i , ... , b_i}
        // d` stores the donations that have been taken already.
        uint64_t d_prime[NUM_SITES];
        for (int i = 0; i < NUM_SITES; i++) d_prime[i] = 0;
        // 4: TempCollected := 0, Time := t, ArrivalTime := t
        // 5: ChosenNode := 0, PreviousNode := 0
        int temp_collected = 0, _time = t, arrival_time = t, chosen_node = 0,
            previous_node = last_node;
        // 6: while Time >= T_1 - K do
        while (_time >= data.first_opening_time - TIME_TO_SPOIL and _time >= 0) {
 
            // 7: for each i ∈ V j do
                /* 8: Run GH with the last node visited i and the visiting
                time Time − t_{i,PreviousNode} and return the total number of
                donations collected (say, Total MaximumCollected i ) */
            // 9: end for
            // 10: ChosenNode := argmax_(i in V_j) TotalMaximumCollected_i


            int total_maximum_collectible = -1;
            int chosen_node         = 0;
            
            for (int site: cluster_indexes){
                if (site == previous_node) continue;
                int gh_collected;
                int gh_endtime = _time - data.get_distance(previous_node, site);
                dprint = false;
                greedy_heuristic(data, cluster, gh_collected, site, gh_endtime, NULL);
                dprint = true;
                if (gh_collected > total_maximum_collectible){
                    total_maximum_collectible = gh_collected;
                    chosen_node = site;
                }
            }

            // 10: MaximumCollectible := max_(i in V_j)
            // sum_(l=ArrivalTime-K)^(Time-t_(i,PreviousNode))d`^ChosenNode_j


            uint64_t collected_donations_set = 0;
            int maximum_collectible = 0;
            for (int donation = 0; donation < data.donation_count[chosen_node]; donation ++) {
                bool has_been_taken = set_has(d_prime[chosen_node],donation);
                int dist_to_prev_node = data.get_distance(chosen_node,previous_node);
                if (!has_been_taken and
                    (data.get_donation(chosen_node,donation) <=
                        _time - dist_to_prev_node) and
                    (data.get_donation(chosen_node,donation) >= arrival_time - TIME_TO_SPOIL)) {
                    maximum_collectible += 1;
                    collected_donations_set |= (1 << donation);
                }
            }
 
            // 12: if MaximumCollectible = 0 & PreviousNode != 0 then
            if (maximum_collectible == 0 and previous_node != 0) {
                // Nowhere to go, let's end the tour.
                //dprintf("End tour\n");
                // Store the current arrival time.
                NodeArrival node_arrival = {previous_node, arrival_time};
                if (node_arrivals) node_arrivals->push_back(node_arrival);

                // 13: ChosenNode := 0, ArrivalTIme := Time - t_(ChosenNode,
                // PreviousNode)
                // 14: Time := ArrivalTime, PreviousNode := 0
                chosen_node   = 0;
                arrival_time  = _time - data.get_distance(0,previous_node);
                _time         = arrival_time;
                previous_node = 0;
            }
            // 15: else if MaximumCollectible = 0 and PreviousNode = 0 then
            else if (maximum_collectible == 0) {
                // Nowhere good to go from the processing center, let's wait a
                // bit.
                arrival_time = arrival_time - LAMBDA; // Bug in the pseudocode, let's update the arrival time on the processing center too.
                // 16: Time := ArrivalTime - lambda

                _time = arrival_time;
                continue;
            }
            // 17: else
            else {
                // There's somewhere to go! Let's go there.

                // Store the current arrival time.


                NodeArrival node_arrival = {previous_node, arrival_time};
                if (node_arrivals) node_arrivals->push_back(node_arrival);

                // 18: TempCollected := TempCollected + MaximumCollectible
                temp_collected += maximum_collectible;
                
                // 19: for l = ArrivalTime - K to Time - t_(ChosenNode,
                // PreviousNode) do
                // 20: d`^ChosenNode_l := 0
                // 21: end for
                d_prime[chosen_node] |= collected_donations_set;

                // 22: Time := Time - t_(ChosenNode, PreviousNode)
                _time -= data.get_distance(chosen_node,previous_node);
                //dprintf("Chosen node %d, grabbed %d donations at time %d, arrival time %d\n", chosen_node, maximum_collectible, _time, arrival_time);
                // 23: PreviousNode := ChosenNode
                previous_node = chosen_node;
                // 24: end if
            }
            // 25: end while
            // 26: if TempCollected > MaximumCollected then
            if (temp_collected > maximum_collected) {
                // 27: MaximumCollected := TempCollected, EndTime := t
                maximum_collected = temp_collected;
            }
            // 28: end if
        }
        // 29: end for
        //dprintf("Collected %d donations when arriving at time %d\n", temp_collected, t);
    }
    //dprintf("Received %d donations from cluster %s\n", maximum_collected, to_list(cluster).c_str());
    indent_level --;
}


// Algorithm 2: Clustering and routing integration

typedef void(*Strategy) (Data&, Set, int&, int, int, std::vector<NodeArrival>*);

void cluster_and_route(Data& data, Set* clusters, Strategy strat){
    dprintf("cluster_and_route:\n");
    // 1: V_j = {} for all j in {1, 2 .. .M}
    int remaining;
    // 2: Choose a cluster center, say v_i in V, for each cluster j using Algoritm 1 and set
    // V_j = {v_j} for all j in {1, 2, ... M}
    // 3: Update the set of remaining sites: V := V \ {v_1, v_2 ... v_M}
    if (!find_initial_donation_sites(data, clusters, remaining)){
        dprintf("Failed initial cluster seeding\n");
        return;
    }

    int cluster_sizes[NUM_VEHICLES];
    for (int i = 0; i < NUM_VEHICLES; i++) cluster_sizes[i] = 1;

    // 4: Define Collected(V_j) as the maximum amount that can be collected by a vehicle that
    // visits the donation sites in V_j

    // 5: for each i in V do
    for (int node = 1; node < NUM_SITES; node++) if (set_has(remaining, node)) {
        dprintf("Find the best cluster for node %d\n", node);
        // Find the best cluster to attach this node to.
        // (Result depends on order of the nodes, which is arbitrary.)

        // 6:  MarginalBenefit_j := {Collected(V_j U {i}) - Collected(V_j)} for all j in {1,2...M}

        std::vector<int> marginal_benefits;

        for (int cluster = 0; cluster < NUM_VEHICLES; cluster++){
            int collected_current, collected_new;
            int last_node = 0, last_time = data.last_closing_time;
            strat(data, clusters[cluster], collected_current, last_node, last_time, NULL);
            strat(data, set_add(clusters[cluster], node), collected_new, last_node, last_time, NULL);
            marginal_benefits.push_back(collected_new - collected_current);
            dprintf("Marginal benefits for node %d and cluster %s are %d\n", node, to_list(clusters[cluster]).c_str(), marginal_benefits.back());
        }

        // 7: MaximumMarginalBenefit := max_{j in {1,2...M}} MarginalBenefit_j
        // 8: ClusterAssigned := argmax_{j in {1,2...M}} MarginalBenefit_j

        int maximum_marginal_benefit = -999999;
        Set max_solutions_set = 0;

        for (int cluster = 0; cluster < NUM_VEHICLES; cluster++){
            if (cluster_sizes[cluster] >= MAX_CLUSTER_SIZE) continue;
            if (marginal_benefits[cluster] > maximum_marginal_benefit){
                maximum_marginal_benefit = marginal_benefits[cluster];
                max_solutions_set = set_add(0,cluster);
            }
            else if (marginal_benefits[cluster] == maximum_marginal_benefit){
                max_solutions_set = set_add(max_solutions_set, cluster);
            }
        }

        dprintf("Maximal clusters are %s with marginal benefit %d\n",to_list(max_solutions_set).c_str(), maximum_marginal_benefit);

        assert(max_solutions_set != 0);
        
        std::vector<int> max_clusters;
        for (int i = 0; i < NUM_VEHICLES; i++){
            if (set_has(max_solutions_set, i)){
                max_clusters.push_back(i);
            }
        }

        // Get one of the maximal solutions at random

        int chosen_cluster = max_clusters[rand() % max_clusters.size()];

        dprintf("Best marginal benefit for node %d is cluster %s\n", node, to_list(clusters[chosen_cluster]).c_str());

        // 9: if MaximumMarginalBenefit > 0 then
        // Some nodes were being left behind. Add an =.
        if (maximum_marginal_benefit >= 0){
            // 10: V_ClusterAssigned := V_ClusterAssigned U {i}
            clusters[chosen_cluster] = set_add(clusters[chosen_cluster], node);
            cluster_sizes[chosen_cluster] += 1;
        }
        // 11: end if
    }
    // 12: end for
}

int upper_bound_calculation(Data &data){
    int rv = 0;
    for (int site = 1; site < NUM_SITES; site++){
        std::vector<int> weights(T2,0);

        // Populate weights. Since donations are sorted, we can do this in O(n).
        int l = 0, r=0, sum = (data.donation_count[site] > 0);
        for (int t = 0; t < T2; t++){
            while (r < data.donation_count[site] - 1 and t >= data.get_donation(site,r+1)){
                r++;
                sum++;
            }
            while (data.get_donation(site,l) < t + data.get_distance(site,0) - TIME_TO_SPOIL){
                sum -= 1;
                l++;
            }
            if (l > r) break;
            if (t < data.get_donation(site,r)) {
                weights[t] = 0;
            }
            else {
                weights[t] = sum;
            }
        }

        // Calculate maximum independent set

        // dp[n] is the maximum donations collectable from time 0 to n
        std::vector<int> dp(T2,0);
        dp[0] = weights[0];
        for (int i = 1; i < T2; i++){
            int no_include = dp[i-1];
            int include = weights[i];
            if (i > 2 * data.get_distance(site,0)){
                include += dp[i- 2 * data.get_distance(site,0)];
            }
            dp[i] = std::max(no_include,include);
        }

        rv += dp[T2-1];
    }
    return rv;
}

int main() {
    Data* data = (Data*) malloc(sizeof(Data));
    int clusters[NUM_VEHICLES];
    std::vector<int> upper_bounds;
    std::vector<int> gh_output;
    std::vector<int> grh_output;
    int num_epochs = 100;
    for (int epoch = 0; epoch < num_epochs; epoch++){
        generate_dataset(data);
        // dprintf("Dataset generated.\n");
        cluster_and_route(*data, clusters, greedy_heuristic);
        // printf("Chosen clusters are:\n");
        int acc = 0;
        for (int i = 0; i < NUM_VEHICLES; i++){
            int maximum_collected;
            greedy_heuristic(*data, clusters[i], maximum_collected, 0, data->last_closing_time, NULL);
            // printf("%s = %d\n",to_list(clusters[i]).c_str(), maximum_collected);
            acc += maximum_collected;
        }
        // printf("Total collected = %d\n", acc);

        gh_output.push_back(acc);
        cluster_and_route(*data, clusters, greedy_recursive_heuristic);
        // printf("Chosen clusters are:\n");
        acc = 0;
        for (int i = 0; i < NUM_VEHICLES; i++){
            int maximum_collected;
            greedy_recursive_heuristic(*data, clusters[i], maximum_collected, 0, data->last_closing_time, NULL);
            // printf("%s = %d\n",to_list(clusters[i]).c_str(), maximum_collected);
            acc += maximum_collected;
        }
        // printf("Total collected = %d\n", acc);
        grh_output.push_back(acc);        
        upper_bounds.push_back(upper_bound_calculation(*data));
    }

    double gh_avg = 0, grh_avg = 0;
    for (int i = 0; i < num_epochs; i++){
        gh_avg += (double) gh_output[i] / (double) upper_bounds[i];
        grh_avg += (double) grh_output[i] / (double) upper_bounds[i];
    }
    gh_avg /= num_epochs;
    grh_avg /= num_epochs;

    printf("Average quality of GH:  %2.3lf%%\n",gh_avg * 100.0);
    printf("Average quality of GRH: %2.3lf%%\n",grh_avg * 100.0);


    return 0;
}