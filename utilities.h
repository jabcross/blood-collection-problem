#include <algorithm>


double gauss() {
    // Return a float centered on 0 with standard deviation 1.
    // Clamp to -0.5 and +0.5.
    static std::default_random_engine       generator;
    static std::normal_distribution<double> dist(0.0, 1.0);
    double                                  rv;
    while (true) {
        rv = dist(generator);
        if (rv >= -0.5 and rv <= 0.5) { break; }
    }
    return rv;
}

void hist(int buckets, int samples) {
    std::vector<double> s;
    double              m = INFINITY;
    double              M = -INFINITY;
    for (int i = 0; i < samples; i++) {
        double x = gauss();
        s.push_back(x);
        m = std::min(m, x);
        M = std::max(M, x);
    }
    printf("min = %lf, max = %lf", m, M);
    std::vector<int> b(buckets);
    for (auto x : s) b[(int) ((x - m) / (M - m) * buckets)]++;
    for (auto y : b) printf("[%d]", y);
    printf("\n");
}

int random_range(int a, int b) {
    return rand() % (b - a) + a;
}

std::string to_binary(int x){
    std::string rv = "";
    for (int i = 31; i >= 0; i--){
        rv.push_back(((x >> i) & 1) + '0');
    }
    return rv;
}

// Sets are bitsets here.

#define Set int
#define set_add(set, node) (set | (1 << node))
#define set_remove(set, node) (set & ~(1 << node))
#define set_subtract(set_a, set_b) (set_a & ~set_b)
#define set_union(set_a, set_b) (set_a | set_b)
#define set_has(set, node) (set & (1 << node))
#define T(...) std::make_tuple(__VA_ARGS__)

std::string to_list(int set){
    std::string rv = "[";
    for (int i = 0; i < 32; i++){
        if set_has(set, i){
            rv += std::to_string(i) + ", ";
        }
    }
    rv += "]";
    return rv;
}

int set_count(uint64_t set){
    int acc = 0;
    for (int i = 0; i < 64; i++){
        if (set_has(set,i)) acc ++;
    }
    return acc;
}

int set_count(Set set){
    int acc = 0;
    for (int i = 0; i < 32; i++){
        if (set_has(set,i)) acc ++;
    }
    return acc;
}